package org.test;


import com.vaadin.ui.Button;

public class Controller implements Button.ClickListener {
   private MyUI view;


    public Controller(MyUI view) {
        this.view=view;
    }

    @Override
    public void buttonClick(Button.ClickEvent clickEvent) {
           view.getOutputLabel().setValue("Submitted!");
    }
}
