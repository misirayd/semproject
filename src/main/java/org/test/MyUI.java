package org.test;

import javax.servlet.annotation.WebServlet;
import javax.swing.*;

import com.vaadin.annotations.Theme;
import com.vaadin.annotations.VaadinServletConfiguration;
import com.vaadin.annotations.Widgetset;
import com.vaadin.client.ui.VPasswordField;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinServlet;
import com.vaadin.ui.*;
import com.vaadin.ui.Button.ClickEvent;

/**
 *
 */
@Theme("mytheme")
@Widgetset("org.test.MyAppWidgetset")
public class MyUI extends UI {
    private Label outputLabel = new Label();
    private Controller controller=null;
    @Override
    protected void init(VaadinRequest vaadinRequest) {
        final VerticalLayout layout = new VerticalLayout();
        controller= new Controller(this);
        final TextField name = new TextField();
        name.setCaption("Enter username");

        final PasswordField pass = new PasswordField();
        pass.setCaption("Enter password");

        Button submitButton = new Button("Submit");
        submitButton.addClickListener(controller);
        
        layout.addComponents(name,pass, submitButton);
        layout.addComponent(outputLabel);
        layout.setMargin(true);
        layout.setSpacing(true);
        
        setContent(layout);
    }
    public Label getOutputLabel() {
        return outputLabel;
    }

    public void setOutputLabel(Label outputLabel) {
        this.outputLabel = outputLabel;
    }
    @WebServlet(urlPatterns = "/*", name = "MyUIServlet", asyncSupported = true)
    @VaadinServletConfiguration(ui = MyUI.class, productionMode = false)
    public static class MyUIServlet extends VaadinServlet {
    }
}
